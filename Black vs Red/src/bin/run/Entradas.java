package bin.run;

import java.util.Scanner;

public class Entradas {

	private static Scanner reader = new Scanner(System.in);

	public static char readChar(String out) {
		char a = 'A';
		boolean ok = false;
		while(!ok) {
			try {
				System.out.print(out);
				a = reader.next().charAt(0);
				if(a >= 'a' && a <= 'z') a = (char) (a - 32);
				if(a >= 'A' && a <= 'Z') ok = true;
			}
			catch(Exception e) {
				System.out.println("Solo se permiten caracteres.");
				reader.nextLine();
			}
		}
		return a;
	}


}

package bin.clases.armas;

import java.awt.geom.Rectangle2D;

import bin.clase.unitys.*;

public abstract class Municion {
	
	private String nombre;
	private int damage;
	private int posX;
	private int posY;
	private boolean alied;
	private int width;
	private int height;
	private int speed;
	
	public Municion(String nombrem, int damage, boolean alied, int width, int height, Ship padre, int speed) {
		this.setNombre(nombrem);
		this.setDamage(damage);
		this.alied = alied;
		this.width = width;
		this.height = height;
		this.setSpeed(speed);
	}
	
	public int getSpeed() {
		return this.speed;
	}
	
	public void setSpeed(int speed) {
		this.speed = speed;
	}
	
	public String getNombre() {
		return this.nombre;
	}
	
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	public int getDamage() {
		return this.damage;
	}
	
	public void setDamage(int damage) {
		this.damage = damage;
	}
	
	public int getPosX() {
		return this.posX;
	}
	
	public void setPosX(int posX) {
		this.posX = posX;
	}
	
	public int getPosY() {
		return this.posY;
	}
	
	public void setPosY(int posY) {
		this.posY = posY;
	}
	
	public boolean isAlied() {
		return this.alied;
	}
	
	public int getWidth() {
		return this.width;
	}
	
	public int getHeight() {
		return this.height;
	}
	
	protected abstract void calcPos(Ship padre);
	
	public abstract boolean move(Ship enemy);
	
	public Rectangle2D getMunicion() {
		return new Rectangle2D.Double(this.posX, this.posY, this.width, this.height);
	}
	
}

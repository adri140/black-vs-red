package bin.clase.tablero;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import javax.swing.JPanel;

import bin.clase.unitys.*;
import bin.clases.armas.*;

public class Tablero extends JPanel {
	
	private List<Ship> b;
	private List<Municion> muni;
	public static final int heightMar = 500 - 280; //altura a la que esta el mar
	public static final int widthMar = 800; //largo del mar
	
	public Tablero() {
		b = new ArrayList<Ship>();
		muni = new ArrayList<Municion>();
		this.setBackground(Color.GRAY);
	}
	
	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		Graphics2D g2 = (Graphics2D) g;
		g2.setColor(Color.WHITE);
		this.dibujar(g2);
		this.actualizar();
	}
	
	public void dibujar(Graphics2D g) {//dibujamos los barcos
		for(Ship s: this.b) {
			if(s.isAlied()) g.setColor(Color.BLACK);
			else g.setColor(Color.RED);
			g.fill(s.getShip());
		}
		Rectangle2D rec = new Rectangle2D.Double(0, this.heightMar, this.widthMar, 2);
		g.setColor(Color.BLUE);
		g.fill(rec);
		
		for(Municion e: muni) {
			if(e.isAlied()) g.setColor(Color.BLACK);
			else g.setColor(Color.RED);
			g.fill(e.getMunicion());
		}
		/*if(this.b != null) {
			if(b.isAlied()) g.setColor(Color.BLACK);
			else g.setColor(Color.RED);
			g.fill(this.b.getShip());
		}*/
	}
	
	/*public void actualizar() { //actualizamos la posicion del barco
		Iterator<Ship> e = this.b.iterator();*/
		/*while(e.hasNext()) {
			Ship s = (Ship) e.next();
			boolean delete = s.move(this.getBounds());
			if(delete) e.remove();
		}*/
		/*if(b != null) {
			boolean delete = this.b.move(this.getBounds());
			if(delete) this.b = null;
		}
		else System.out.println("No quedan barcos");*/
	//}
	
	public void actualizar() { //actualizamos la posicion del barco
		
		List<Ship> aliados = new ArrayList<Ship>();
		List<Ship> enemigos = new ArrayList<Ship>();
		
		//separamos los barcos por el lado del conflicto en el que estan
 		for(Ship e: this.b) {
 			
			if(e.isAlied()) aliados.add(e);
			else enemigos.add(e);
		}
 		
 		//ordenamos las list, en caso de los aliados de mas a menos, los enemigos de menos a mas, de tal forma que los que esten mas cerca de pegarse sean los de las puntas
 		Collections.sort(aliados, Ship.COMPARAR_PARA_ALIADOS);
 		Collections.sort(enemigos, Ship.COMPARAR_PARA_ENEMIGOS);
 		
 		//bucle, en el qual comprovamos si hay algun aliado cerca de algun barco enemigo, si lo hay y puede atacar-le le atacara, si no pasara
 		Iterator<Ship> e = aliados.iterator();
 		boolean delete;
 		while(e.hasNext()) {
 			delete = false;
 			Ship s = e.next();
 			if(enemigos.size() > 0) {
	 			if(enemigos.get(0).getPosX() - (s.getPosX() + s.getAncho()) <= s.getSpaceAtac()) { //calcula si se tiene que parar o no
	 				delete = s.move(this.getBounds(), true, enemigos.get(0), this);
	 			}
	 			else {
	 				delete = s.move(this.getBounds(), false, null, this);
	 			}
 			}
 			else {
 				delete = s.move(this.getBounds(), false, null, this);
 			}
 			if(delete) e.remove();
 		}
 		
 		//lo mismo para los enemigos
 		e = enemigos.iterator();
 		while(e.hasNext()) {
 			delete = false;
 			Ship s = e.next();
 			if(aliados.size() > 0) {
	 			if(s.getPosX() - (aliados.get(0).getPosX() + aliados.get(0).getAncho()) <= s.getSpaceAtac()) {
	 				delete = s.move(this.getBounds(), true, aliados.get(0), this);
	 			}
	 			else {
	 				delete = s.move(this.getBounds(), false, null, this);
	 			}
 			}
 			else {
 				delete = s.move(this.getBounds(), false, null, this);
 			}
 			if(delete) e.remove();
 		}
 		
 		Iterator<Ship> er = this.b.iterator();
 		while(er.hasNext()) {
 			Ship s = er.next();
 			boolean esta = false;
 			e = aliados.iterator();
 			while(e.hasNext() && !esta) {
 				Ship al = e.next();
 				if(al.equals(s)) esta = true;
 			}
 			
 			e = enemigos.iterator();
 			while(e.hasNext() && !esta) {
 				Ship al = e.next();
 				if(al.equals(s)) esta = true;
 			}
 			if(!esta) er.remove();
 		}
 		
 		Iterator<Municion> m = muni.iterator();
 		while(m.hasNext()) {
 			delete = false;
 			Municion mm = m.next();
 			if(mm.isAlied())	{
 				if(enemigos.size() > 0) delete = mm.move(enemigos.get(0));
 				else m.remove();
 			}
 			else {
 				if(aliados.size() > 0) delete = mm.move(aliados.get(0));
 				else m.remove();
 			}
 			if(delete) m.remove();
 		}	
	}
	
	public void addShip(char a) {
		if(this.b != null) {
			switch(a) {
			case 'A':
				this.b.add(new Destroyer(true));
				break;
			case 'B':
				this.b.add(new Destroyer(false));
				break;
			case 'C':
				this.b.add(new Submarine(true));
				break;
			case 'D':
				this.b.add(new Submarine(false));
				break;
			}
		}
	}
	
	/*public int getHeightMar() {
		return this.heightMar;
	}*/
	
	public List<Ship> getShips(){
		return this.b;
	}
	
	public void addMunicion(Municion m) {
		this.muni.add(m);
	}
	
}

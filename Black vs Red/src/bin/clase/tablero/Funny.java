package bin.clase.tablero;

public class Funny extends Thread {
	
	private Tablero tab;
	
	public Funny(Tablero tab) {
		this.tab = tab;
	}
	
	@Override
	public void run() {
		while(true) {
			try {
				Thread.sleep(60); //50 default tmp
			}
			catch(InterruptedException e) {
				e.printStackTrace();
			}
			this.tab.repaint();
		}
	}
	
}

package bin.clase.tablero;

import javax.swing.JFrame;

public class Window extends JFrame {

	private final int ancho = 800, alto = 500;
	private Tablero tab;
	private Funny fun;
	
	public Window() {
		this.setTitle("En desarrollo"); //introducimos el nombre de la ventana
		this.setSize(this.ancho, this.alto); //indicamos el ancho y alto de la ventana
		this.setLocationRelativeTo(null); //colocamos la ventana en el centro
		this.setResizable(false); //no permitimos que se modifique el tamanyo de la pantalla
		this.tab = new Tablero();
		this.add(this.tab);
		
		fun = new Funny(this.tab);
		fun.start();
	}
	
	public Tablero getTab() {
		return this.tab;
	}
}

package bin.clase.unitys;

import java.awt.Rectangle;

import bin.clase.tablero.Tablero;
import bin.clases.armas.Torpedo;

public class Submarine extends Ship implements Underwater {
	
	private int elapsus = 16;
	private int time = 0;
	
	public Submarine(boolean alied) {
		super("submarine", 2, 100, alied, 30, 8, 28);
		this.setInicialPosY();
	}
	
	private void setInicialPosY() {
		if(this instanceof Navigate) super.setPosY(500 - 290);
		if(this instanceof Underwater) super.setPosY(500 - 200);
		if(this instanceof Fly) super.setPosY(500 - 360);
	}
	
	@Override
	public boolean move(Rectangle limites, boolean ship, Ship enemy, Tablero tab) {
		boolean delete = false;
		
		if(ship) super.desincrementarVel();
		else super.incrementarVel();
		
		if(super.isAlied())	super.setPosX(super.getPosX() + super.getSpeed());
		else super.setPosX(super.getPosX() - super.getSpeed());
		
		//si enemigo es true disparamos
		if(ship) {
			if(this.time == 0) {
				tab.addMunicion(new Torpedo(super.isAlied(), this));
				this.time++;
			}
			else {
				this.time++;
				if(this.time == this.elapsus) this.time = 0;
			}
		}
				
		//comprovamos limites y la vida
		if(super.getLive() <= 0) delete = true;
		if(super.getPosX() > 800 && super.isAlied()) delete = true;
		if(super.getPosX() < 0 && !super.isAlied()) delete = true;
				
		return delete;
	}
}

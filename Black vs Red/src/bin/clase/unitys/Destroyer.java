package bin.clase.unitys;

import java.awt.Rectangle;

import bin.clase.tablero.Tablero;
import bin.clases.armas.Torpedo;

public class Destroyer extends Ship implements Navigate {
	
	private int elapsus = 14; //indica las bueltas de bucle que tiene que pasar hasta que vuelva a atacar
	private int time = 0; //contador para elapsus
	
	public Destroyer(boolean alied) {
		super("Destroyer", 4, 100, alied, 28, 12, 40);
		this.setInicialPosY();
	}
	
	private void setInicialPosY() {
		if(this instanceof Navigate) super.setPosY(500 - 290);
		if(this instanceof Underwater) super.setPosY(500 - 200);
		if(this instanceof Fly) super.setPosY(500 - 360);
	}
	
	/*@Override
	public boolean move(Rectangle limites) {
		boolean delete = false;
		if(super.isAlied()) {
			if(limites.getMaxX() > this.getPosX()) this.setPosX(this.getPosX() + this.getSpeed());
			else delete = true;
		}
		else {
			if(-10 < this.getPosX()) this.setPosX(this.getPosX() - this.getSpeed());
			else delete = true;
		}
		return delete;
	}*/
	
	@Override
	public boolean move(Rectangle limites, boolean ship, Ship enemy, Tablero tab) {
		boolean delete = false;
		
		if(ship) { //atacar
			super.desincrementarVel();
		}
		else { //mover
			super.incrementarVel();
		}
		
		//calculamos la nueva posicion
		if(super.isAlied())	super.setPosX(super.getPosX() + super.getSpeed());
		else super.setPosX(super.getPosX() - super.getSpeed());
		
		//si enemigo es true disparamos
		if(ship) {
			if(this.time == 0) {
				tab.addMunicion(new Torpedo(super.isAlied(), this));
				this.time++;
			}
			else {
				this.time++;
				if(this.time == this.elapsus) this.time = 0;
			}
		}
		
		//comprovamos limites y la vida
		if(super.getLive() <= 0) delete = true;
		if(super.getPosX() > 800 && super.isAlied()) delete = true;
		if(super.getPosX() < 0 && !super.isAlied()) delete = true;
		
		return delete;
	}
}

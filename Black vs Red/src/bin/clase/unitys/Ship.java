package bin.clase.unitys;

import java.awt.Rectangle;
import java.awt.geom.Rectangle2D;
import java.util.Comparator;

import bin.clase.tablero.Tablero;

public abstract class Ship {

		private String name; //nombre del barco
		private int speed; //velocidad del barco, cuanto habanza la x por cada vuelta del bucle, su velocidad actual
		private int live; //vida del barco
		private int posX; //posicion X en el tablero
		private int posY; //posicion Y en el tablero
		private boolean alied; //si es aliado o no, indica el bando del barco
		private int ancho;
		private int alto;
		private int speedBasic;//velocidad vasica del barco
		private int spaceAtac; //espacio que hay entre un barco i otro, dependiendo del barco sera mallor o menor, esto quizas deberia ser segun el tipo de municion que utilize el barco
		
		public Ship(String name, int speed, int live, boolean alied, int ancho, int alto, int spaceAtac) {
			this.setName(name);
			this.setSpeed(speed);
			this.setLive(live);
			this.setAlied(alied);
			if(this.alied) this.setPosX(-10);
			else this.setPosX(810);
			this.ancho = ancho;
			this.alto = alto;
			this.speedBasic = this.speed;
			this.spaceAtac = spaceAtac;
		}
		
		public String getName() {
			return this.name;
		}
		
		public int getSpeed() {
			return this.speed;
		}
		
		public int getLive() {
			return this.live;
		}
		
		public int getPosX() {
			return this.posX;
		}
		
		public int getPosY() {
			return this.posY;
		}
		
		public boolean isAlied() {
			return this.alied;
		}
		
		public void setName(String name) {
			this.name = name;
		}
		
		public void setSpeed(int speed) {
			this.speed = speed;
		}
		
		public void setLive(int live) {
			this.live = live;
		}
		
		public void setPosX(int pos) {
			this.posX = pos;
		}
		
		public void setPosY(int pos) {
			this.posY = pos;
		}
		
		public void setAlied(boolean alied) {
			this.alied = alied;
		}
		
		public int getAncho() {
			return this.ancho;
		}
		
		public int getAlto() {
			return this.alto;
		}
		
		public void atacado(int da�o) {
			this.setLive(this.getLive() - da�o);
		}
		
		public int getSpaceAtac() {
			return this.spaceAtac;
		}
		
		/**
		 * Permita mover la posicion X del barco, este metodo sera sobrescrito por los barcos
		 * @param limites
		 * import the limits of window.
		 * @return 
		 * <ul>
		 * 	<li>true: delete this ship</li>
		 * 	<li>false: don't delete this ship</li>
		 * </ul>
		 */
		public boolean move(Rectangle limites) {
			boolean delete = false;
			if(alied) {
				if(limites.getMaxX() > this.getPosX()) this.setPosX(this.getPosX() + this.getSpeed());
				else delete = true;
			}
			else {
				if(-30 < this.getPosX()) this.setPosX(this.getPosX() - this.getSpeed());
				else delete = true;
			}
			return delete;
		}
		
		public Rectangle2D getShip() {
			return new Rectangle2D.Double(this.posX, this.posY, this.ancho, this.alto);
		}
		
		//falta^^^^^^
		
		/**
		 * 	Comparator para aliados, compara por x
		 */
		public static Comparator<Ship> COMPARAR_PARA_ALIADOS = new Comparator<Ship>() {
			@Override
			public int compare(Ship arg0, Ship arg1) {
				int ship1 = arg0.getAncho() + arg0.getPosX();
				int ship2 = arg1.getAncho() + arg1.getPosX();
				return ship2 - ship1;
			}
		};
		
		/**
		 * Comparador pasa enemigos, compara por x
		 */
		public static Comparator<Ship> COMPARAR_PARA_ENEMIGOS = new Comparator<Ship>() {
			@Override
			public int compare(Ship o1, Ship o2) {
				int ship1 = o1.getPosX();
				int ship2 = o2.getPosX();
				return  ship1 - ship2;
			}
		};
		
		public abstract boolean move(Rectangle limites, boolean ship, Ship enemy, Tablero tab);
		
		@Override
		public String toString() {
			return "PosX: " + this.getPosX() + " PosY: " + this.getPosY() + " nombre: " + this.getName();
		}
		
		public void incrementarVel() {
			if(this.speed < this.speedBasic) {
				this.speed = this.speed + 2;
			}
		}
		
		public void desincrementarVel() {
			if(this.speed > 0) {
				this.speed = this.speed - 2;
			}
		}
}

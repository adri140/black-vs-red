package bin.run;

import javax.swing.JFrame;

import bin.clases.tablero.*;

public class Run {
	
	public static void main(String[] args) {
		System.out.println("En desrrollo...");
		Window win = new Window();
		win.setVisible(true); //hacemos la ventana visible
		win.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); //salir al presionar X
		Tablero tab = win.getTab();
		
		//temporal, usado para hacer pruebas, mediante consola y ventana
		System.out.println("Introduce barcos: ");
		while(true) {
			char a = Entradas.readChar("");
			tab.addShip(a);
			System.out.println("Numero de barcos en movimiento: " + tab.getShips().size());
		}
	}
}

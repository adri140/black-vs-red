package bin.clases.tablero;

public class Funny extends Thread {
	
	private Tablero tab;
	
	//constructor
	public Funny(Tablero tab) {
		this.tab = tab;
	}
	
	//metodo que realiza el funcionamiento del juego
	@Override
	public void run() {
		while(true) {
			try {
				Thread.sleep(70); //50 default tmp
			}
			catch(InterruptedException e) {
				e.printStackTrace();
			}
			this.tab.repaint();
		}
	}
	
}

package bin.clases.tablero;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import javax.swing.JPanel;

import bin.clases.unity.*;
import bin.clases.armas.*;

public class Tablero extends JPanel {

	private List<Ship> ships;
	private List<Municion> municion;
	//altura a la que se encuentra la linea del mar.
	public static final int heightMar = 500 - 280; 
	//tamanyo del mar
	public static final int widthMar = 800;
	
	//constructor
	public Tablero() {
		this.ships = new ArrayList<Ship>();
		for(int i = 0; i < 2; i++) {
			if(i < 1) this.ships.add(new CreaShips(true));
			else this.ships.add(new CreaShips(false));
		}
		this.municion = new ArrayList<Municion>();
		this.setBackground(Color.GRAY);
	}
	
	//pinta los componentes del tablero, utiliza el metodo actualizar para actualizar los objetos y dibujar para dibujar cada opjeto.
	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		Graphics2D g2 = (Graphics2D) g;
		g2.setColor(Color.WHITE);
		this.dibujar(g2);
		this.actualizar();
	}
	
	//dibuja cada objeto de las listas, llamando a su metodo getShip y getMunicion 
	public void dibujar(Graphics2D g) {//dibujamos los barcos
		
		//dibujamos los barco
		for(Ship s: this.ships) {
			if(s.isAlied()) g.setColor(Color.BLACK);
			else g.setColor(Color.RED);
			g.fill(s.getShip());
		}
		
		//dibujamos el mar
		Rectangle2D rec = new Rectangle2D.Double(0, this.heightMar, this.widthMar, 2);
		g.setColor(Color.BLUE);
		g.fill(rec);
		
		//dibujamos la municion
		for(Municion e: this.municion) {
			if(e.isAlied()) g.setColor(Color.BLACK);
			else g.setColor(Color.RED);
			g.fill(e.getMunicion());
		}
	}
	
public void actualizar() { //actualizamos la posicion de los barcos
		
		List<Ship> aliados = new ArrayList<Ship>();
		List<Ship> enemigos = new ArrayList<Ship>();
		
		//separamos los barcos por el lado del conflicto en el que estan
 		for(Ship e: this.ships) {
 			
			if(e.isAlied()) aliados.add(e);
			else enemigos.add(e);
		}
 		
 		//ordenamos las list, en caso de los aliados de mas a menos, los enemigos de menos a mas, de tal forma que los que esten mas cerca de pegarse sean los de las puntas
 		Collections.sort(aliados, Ship.COMPARAR_PARA_ALIADOS);
 		Collections.sort(enemigos, Ship.COMPARAR_PARA_ENEMIGOS);
 		
 		//bucle, en el qual comprovamos si hay algun aliado cerca de algun barco enemigo, si lo hay y puede atacar-le le atacara, si no pasara
 		Iterator<Ship> e = aliados.iterator();
 		boolean delete;
 		while(e.hasNext()) {
 			delete = false;
 			Ship s = e.next();
 			if(enemigos.size() > 0) {
	 			if(enemigos.get(0).getPosX() - (s.getPosX() + s.getWidth()) <= s.getSpaceAtac()) { //calcula si se tiene que parar o no
	 				delete = s.move(this.getBounds(), true, enemigos.get(0), this);
	 			}
	 			else delete = s.move(this.getBounds(), false, null, this);
 			}
 			else delete = s.move(this.getBounds(), false, null, this);
 			if(delete) e.remove();
 		}
 		
 		//lo mismo para los enemigos
 		e = enemigos.iterator();
 		while(e.hasNext()) {
 			delete = false;
 			Ship s = e.next();
 			if(aliados.size() > 0) {
	 			if(s.getPosX() - (aliados.get(0).getPosX() + aliados.get(0).getWidth()) <= s.getSpaceAtac()) delete = s.move(this.getBounds(), true, aliados.get(0), this);
	 			else delete = s.move(this.getBounds(), false, null, this);
 			}
 			else delete = s.move(this.getBounds(), false, null, this);
 			if(delete) e.remove();
 		}
 		
 		Iterator<Ship> er = this.ships.iterator();
 		while(er.hasNext()) {
 			Ship s = er.next();
 			boolean esta = false;
 			e = aliados.iterator();
 			while(e.hasNext() && !esta) {
 				Ship al = e.next();
 				if(al.equals(s)) esta = true;
 			}
 			
 			e = enemigos.iterator();
 			while(e.hasNext() && !esta) {
 				Ship al = e.next();
 				if(al.equals(s)) esta = true;
 			}
 			if(!esta) er.remove();
 		}
 		
 		Iterator<Municion> m = this.municion.iterator();
 		while(m.hasNext()) {
 			delete = false;
 			Municion mm = m.next();
 			if(mm.isAlied())	{
 				if(enemigos.size() > 0) delete = mm.move(enemigos.get(0));
 				else m.remove();
 			}
 			else {
 				if(aliados.size() > 0) delete = mm.move(aliados.get(0));
 				else m.remove();
 			}
 			if(delete) m.remove();
 		}	
	}

	//debuelve la Lista de ships
	public List<Ship> getShips(){
		return this.ships;
	}
	
	//permite a�adir nuevas municiones
	public void addMunicion(Municion m) {
		this.municion.add(m);
	}
	
	//temporal, a�ade barcos a ships
	public void addShip(char a) {
		if(this.ships != null) {
			switch(a) {
			case 'A':
				this.ships.add(new Destroyer(true));
				break;
			case 'B':
				this.ships.add(new Destroyer(false));
				break;
			case 'C':
				this.ships.add(new Submarine(true));
				break;
			case 'D':
				this.ships.add(new Submarine(false));
				break;
			}
		}
	}
	
}

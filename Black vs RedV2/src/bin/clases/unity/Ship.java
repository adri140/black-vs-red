package bin.clases.unity;

import java.awt.Rectangle;
import java.awt.geom.Rectangle2D;
import java.util.Comparator;

import bin.clases.tablero.Tablero;
import bin.clases.points.*;

public abstract class Ship {
	
	//barco
	private String name; //nombre del barco
	private int speedBasic; //velocidad basica del barco
	private int speed; //velocidad actual del barco
	private boolean alied; //indica si el barco es aliado
	private int live; //vida del barco
	
	//atacar a otro barco
	private int spaceAtac; //indica el espacio necesario para poder atacar a otro barco
	
	//posicion y tama�o del barco
	private int posX; //posicion X donde empieza el barco
	private int posY; //posicion Y donde empieza el barco
	private int width; //anchura del barco
	private int height; //altura del barco
	
	//constructor
	public Ship(String name, int speed, int live, boolean alied, int width, int height, int spaceAtac) {
		this.name = name;
		this.speedBasic = speed;
		this.setSpeed(speed);
		this.setLive(live);
		this.alied = alied;
		this.width = width;
		this.height = height;
		this.spaceAtac = spaceAtac;
		
		//posicionamos el barco
		if(this.isAlied()) this.setPosX(-10);
		else this.setPosX(810);
	}
	
	public int getLive() {
		return this.live;
	}
	
	public void setLive(int live) {
		this.live = live;
	}
	
	public String getName() {
		return this.name;
	}
	
	public int getSpeed() {
		return this.speed;
	}
	
	public void setSpeed(int speed) {
		this.speed = speed;
	}
	
	public boolean isAlied() {
		return this.alied;
	}
	
	public int getSpaceAtac() {
		return this.spaceAtac;
	}
	
	public int getPosX() {
		return this.posX;
	}
	
	public void setPosX(int posX) {
		this.posX = posX;
	}
	
	public int getPosY() {
		return this.posY;
	}
	
	public void setPosY(int posY) {
		this.posY = posY;
	}
	
	public int getWidth() {
		return this.width;
	}
	
	public int getHeight() {
		return this.height;
	}
	
	//otros metodos
	
	//resta el da�o recivido a la vida que tiene actualmente
	public void atacado(int da�o) {
		this.setLive(this.getLive() - da�o);
	}
	
	//principales
	/**
	 * Permita mover la posicion X del barco, este metodo sera sobrescrito por los barcos
	 * @param limites
	 * import the limits of window.
	 * @return 
	 * <ul>
	 * 	<li>true: delete this ship</li>
	 * 	<li>false: don't delete this ship</li>
	 * </ul>
	 */
	public boolean move(Rectangle limites) {
		boolean delete = false;
		if(alied) {
			if(limites.getMaxX() > this.getPosX()) this.setPosX(this.getPosX() + this.getSpeed());
			else delete = true;
		}
		else {
			if(-30 < this.getPosX()) this.setPosX(this.getPosX() - this.getSpeed());
			else delete = true;
		}
		return delete;
	}
	
	//devuelve el barco como un objeto rectangulo
	public Rectangle2D getShip() {
		return new Rectangle2D.Double(this.getPosX(), this.getPosY(), this.getWidth(), this.getHeight());
	}
	
	//incrementa la velocidad del barco, usado para quando se destruye un enemigo
	public void incrementarVel() {
		if(this.getSpeed() < this.speedBasic) {
			this.setSpeed(this.getSpeed() + 2);
		}
	}
	
	//desincrementa la velocidad del barco, usado para parar el barco quando se encuentra un enemigo
	public void desincrementarVel() {
		if(this.getSpeed() > 0) this.setSpeed(this.speed - 2);
	}
	
	//obligamos a nuestras subclases crear un metodo move.
	public abstract boolean move(Rectangle limites, boolean ship, Ship enemy, Tablero tab);

	/**
	 * 	Comparator para aliados, compara por x
	 */
	public static Comparator<Ship> COMPARAR_PARA_ALIADOS = new Comparator<Ship>() {
		@Override
		public int compare(Ship arg0, Ship arg1) {
			int ship1 = arg0.getWidth() + arg0.getPosX();
			int ship2 = arg1.getWidth() + arg1.getPosX();
			return ship2 - ship1;
		}
	};
	
	/**
	 * Comparador pasa enemigos, compara por x
	 */
	public static Comparator<Ship> COMPARAR_PARA_ENEMIGOS = new Comparator<Ship>() {
		@Override
		public int compare(Ship o1, Ship o2) {
			int ship1 = o1.getPosX();
			int ship2 = o2.getPosX();
			return  ship1 - ship2;
		}
	};
	
	//calcula todos los puntos que rodean el objeto barco, estos puntos se utilizaran como puntos de colision para la municion Terminar
	private void iniciarPuntos() {
		int totalPuntos = (this.getWidth() * 2) + (this.getHeight() * 2);
		Punto[] puntos = new Punto[totalPuntos];
		int posX = this.getPosX();
		int posY = this.getPosY();
	}
}

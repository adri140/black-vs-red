package bin.clases.unity;

import java.awt.Rectangle;

import bin.clases.tablero.Tablero;
import bin.clases.unitystype.Fly;
import bin.clases.unitystype.Navigate;
import bin.clases.unity.Ship;
import bin.clases.unitystype.Underwater;
import bin.clases.armas.*;

public class Destroyer extends Ship implements Navigate {
	
	private int elapsus = 18; //indica las bueltas de bucle que tiene que pasar hasta que vuelva a atacar
	private int time = 0; //contador para elapsus
	
	//constructor
	public Destroyer(boolean alied) {
		super("Destroyer", 4, 100, alied, 28, 12, 60);
		this.setInicialPosY();
	}
	
	//indica la posicion Y dependiendo el tipo de barco
	private void setInicialPosY() {
		if(this instanceof Navigate) super.setPosY(500 - 290);
		if(this instanceof Underwater) super.setPosY(500 - 200);
		if(this instanceof Fly) super.setPosY(500 - 360);
	}
	
	//mueve el barco, si hay algun enemigo el barco ataca al barco enemigo, en un futuro comprovara si esta al alcance
	@Override
	public boolean move(Rectangle limites, boolean ship, Ship enemy, Tablero tab) {
		boolean delete = false;
		
		if(ship) super.desincrementarVel(); //atacar
		else super.incrementarVel(); //mover
		
		//calculamos la nueva posicion
		if(super.isAlied())	super.setPosX(super.getPosX() + super.getSpeed());
		else super.setPosX(super.getPosX() - super.getSpeed());
		
		//si enemigo es true disparamos
		if(ship) {
			if(this.time == 0) {
				tab.addMunicion(new Torpedo(super.isAlied(), this));
				this.time++;
			}
			else {
				this.time++;
				if(this.time == this.elapsus) this.time = 0;
			}
		}
		
		//comprovamos limites y la vida
		if(super.getLive() <= 0) delete = true;
		if(super.getPosX() > 800 && super.isAlied()) delete = true;
		if(super.getPosX() < 0 && !super.isAlied()) delete = true;
		
		return delete;
	}
}

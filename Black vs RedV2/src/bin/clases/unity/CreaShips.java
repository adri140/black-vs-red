package bin.clases.unity;

import java.awt.Rectangle;

import bin.clases.tablero.Tablero;
import bin.clases.unitystype.*;

public class CreaShips extends Ship implements Navigate, Fly, Underwater {
	
	//private List<Torret> torretas; //los creaships pueden tener torretas,
	
	//constructor
	public CreaShips(boolean alied) {
		super("CreaShip", 0, 10000, alied, 50, 800 - 20, 0);
		this.calcPos();
	}
	
	//calculamos la posicion concreta para los creaships
	private void calcPos() {
		if(super.isAlied()) {
			super.setPosX(0);
			super.setPosY(20);
		}
		else {
			super.setPosX(800 - 50);
			super.setPosY(20);
		}
	}
	
	//el move devuelve false.
	@Override
	public boolean move(Rectangle limites, boolean ship, Ship enemy, Tablero tab) {
		return false;
	}
	
}

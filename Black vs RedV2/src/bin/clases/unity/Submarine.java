package bin.clases.unity;

import java.awt.Rectangle;

import bin.clases.tablero.Tablero;
import bin.clases.unitystype.*;
import bin.clases.armas.*;

public class Submarine extends Ship implements Underwater {
	
	private int elapsus = 16; //tiempo necesario hasta poder lanzar otro torpedo
	private int time; //contador para lanzar otro torpedo
	
	//constructor
	public Submarine(boolean alied) {
		super("submarine", 2, 100, alied, 30, 8, 32);
		this.setInicialPosY();
	}
	
	//iniciador de posicion Y
	private void setInicialPosY() {
		if(this instanceof Navigate) super.setPosY(500 - 290);
		if(this instanceof Underwater) super.setPosY(500 - 200);
		if(this instanceof Fly) super.setPosY(500 - 360);
	}
	
	//metodo que permite mover el submarino
	@Override
	public boolean move(Rectangle limites, boolean ship, Ship enemy, Tablero tab) {
		boolean delete = false;
		
		if(ship) super.desincrementarVel();
		else super.incrementarVel();
		
		if(super.isAlied())	super.setPosX(super.getPosX() + super.getSpeed());
		else super.setPosX(super.getPosX() - super.getSpeed());
		
		//si enemigo es true disparamos
		if(ship) {
			if(this.time == 0) {
				tab.addMunicion(new Torpedo(super.isAlied(), this));
				this.time++;
			}
			else {
				this.time++;
				if(this.time == this.elapsus) this.time = 0;
			}
		}
				
		//comprovamos limites y la vida
		if(super.getLive() <= 0) delete = true;
		if(super.getPosX() > 800 && super.isAlied()) delete = true;
		if(super.getPosX() < 0 && !super.isAlied()) delete = true;
				
		return delete;
	}
}

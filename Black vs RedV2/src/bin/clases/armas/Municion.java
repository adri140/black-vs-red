package bin.clases.armas;

import java.awt.geom.Rectangle2D;

import bin.clases.unity.*;

public abstract class Municion {

	private String name; //nombre de la municion
	private int damage; //da�o de la municion al atacar a un barco
	private boolean alied; //indica si es aliado
	private int speed; //velocidad del torpedo
	
	private int posX; //posicion X 
	private int posY; //posicion Y 
	private int width; //ancho de la municion
	private int height; //altura de la municion
	
	//constructor
	public Municion(String nombrem, int damage, boolean alied, int width, int height, Ship padre, int speed) {
		this.setNombre(nombrem);
		this.setDamage(damage);
		this.alied = alied;
		this.width = width;
		this.height = height;
		this.setSpeed(speed);
	}
	
	public int getSpeed() {
		return this.speed;
	}
	
	public void setSpeed(int speed) {
		this.speed = speed;
	}
	
	public String getNombre() {
		return this.name;
	}
	
	public void setNombre(String name) {
		this.name = name;
	}
	
	public int getDamage() {
		return this.damage;
	}
	
	public void setDamage(int damage) {
		this.damage = damage;
	}
	
	public int getPosX() {
		return this.posX;
	}
	
	public void setPosX(int posX) {
		this.posX = posX;
	}
	
	public int getPosY() {
		return this.posY;
	}
	
	public void setPosY(int posY) {
		this.posY = posY;
	}
	
	public boolean isAlied() {
		return this.alied;
	}
	
	public int getWidth() {
		return this.width;
	}
	
	public int getHeight() {
		return this.height;
	}
	
	//calcula la posicion de X y Y respecto al padre
	protected abstract void calcPos(Ship padre);
	
	//permite mover la municion hacia el enemigo
	public abstract boolean move(Ship enemy);
	
	//devuelve la municion como un objeto rectangulo2D
	public Rectangle2D getMunicion() {
		return new Rectangle2D.Double(this.posX, this.posY, this.width, this.height);
	}
}

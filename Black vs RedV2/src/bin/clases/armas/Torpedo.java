package bin.clases.armas;

import bin.clases.tablero.Tablero;
import bin.clases.unitystype.*;
import bin.clases.unity.*;

public class Torpedo extends Municion implements Underwater {
	
	//constructor
	public Torpedo(boolean alied, Ship padre) {
		super("Torpedo", 20, alied, 6, 2, padre, 5);
		this.calcPos(padre);
	}
	
	//calcula la posicion X y Y del torpedo al crear-lo
	protected void calcPos(Ship padre) {
		if(padre instanceof Navigate) { //destructor
			super.setPosX((padre.getWidth() / 2) + padre.getPosX());
			super.setPosY(padre.getPosY() + padre.getHeight() - 1);
		}
		else {
			if(padre instanceof Underwater) { //submarino
				if(this.isAlied()) {
					super.setPosX(padre.getPosX() + padre.getWidth() + 1);
					super.setPosY(padre.getPosY() + padre.getHeight() - 1);
				}
				else {
					super.setPosX(padre.getPosX() - 1);
					super.setPosY(padre.getPosY() + padre.getHeight() - 1);
				}
			}
		}
	}
	
	//permite mover el torpedo
	@Override
	public boolean move(Ship enemy) {
		
		//posicion objetivo
		int posEnemyX = enemy.getPosX() + (enemy.getWidth() / 2);
		int posEnemyY = enemy.getPosY() + enemy.getHeight();
		
		boolean delete = false;
		int moverX = super.getSpeed();
		int moverY = super.getSpeed();
		
		//calculamos la distancia hasta el objetivo, si el objetovo esta a mas de la distancia que puede recorer, la distancia se resuce
		//eje Y
		if(super.getPosY() == posEnemyY) moverY = 0;
		else {
			if(super.getPosY() > posEnemyY) moverY = super.getPosY() - posEnemyY;
			else moverY = posEnemyY - super.getPosY();
			
			if(moverY > super.getSpeed()) moverY = super.getSpeed();
		}
		
		//eje X
		if(super.getPosX() == posEnemyX) moverX = 0;
		else {
			if(super.getPosX() > posEnemyX) moverX = super.getPosX() - posEnemyX;
			else moverX = posEnemyX - super.getPosX();
			
			if(moverX > super.getSpeed()) moverX = super.getSpeed();
		}
		
		//calculamos la nueva posicion del torpedo
		//eje X
		if(super.getPosX() > posEnemyX) {
			super.setPosX(super.getPosX() - moverX);
		}
		else {
			super.setPosX(super.getPosX() + moverX);
		}
		
		//eje Y
		if(super.getPosY() > posEnemyY) {
			super.setPosY(super.getPosY() - moverY);
		}
		else {
			super.setPosY(super.getPosY() + moverY);
		}
		
		//comprovamos que el torpedo no suba al cielo, TEMPORAL, en un futuro se devera de ver si el enemigo puuede ser atacado por este tipo de municion
		if(super.getPosY() < Tablero.heightMar) {
			super.setPosY((Tablero.heightMar - super.getPosY()) - super.getPosY());
		}
		
		//comprovamos si el enemigo es atacado
		if(moverX == 0 && moverY == 0) {
			delete = true;
			enemy.atacado(super.getDamage());
		}
		else {
			if(super.getPosX() == posEnemyX && super.getPosY() == posEnemyY) {
				enemy.atacado(super.getDamage());
				delete = true;
			}
		}
		//si el enemigo es atacado, le atacaremos, lo cual causara la destruccion de nuestra municion
		
		return delete;	
	}
	
}
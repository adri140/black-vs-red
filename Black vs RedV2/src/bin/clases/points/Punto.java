package bin.clases.points;

public class Punto {
	
	private int posX; //posicion X
	private int posY; //posicion Y
	
	public Punto(int posX, int posY) {
		this.setPosX(posX);
		this.setPosY(posY);
	}
	
	public int getPosX() {
		return this.posX;
	}
	
	public void setPosX(int posX) {
		this.posX = posX;
	}
	
	public int getPosY() {
		return this.posY;
	}
	
	public void setPosY(int posY) {
		this.posY = posY;
	}
	
}
